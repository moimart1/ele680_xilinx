----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:14:58 03/09/2014 
-- Design Name: 
-- Module Name:    module_main - Arch_main 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity module_main is
    Port ( clk_dcm : in  STD_LOGIC;
			  reset : in  STD_LOGIC;
           control : in  STD_LOGIC_VECTOR (7 downto 0);
           data_in : in  STD_LOGIC_VECTOR (15 downto 0);
           data_out : out  STD_LOGIC_VECTOR (15 downto 0); -- A voir
           clk_eno : out  STD_LOGIC); -- A voir
end module_main;

architecture Arch_main of module_main is

	type state_type is (st_nstart, st_set_division, st_generate_clk); 
   signal state, next_state : state_type; 

	signal buff_freq_division : integer range 0 to 31 := 0;
	signal temp_clk: STD_LOGIC;
   signal counter : integer range 0 to 31 := 0;
	--signal buff_data2 : STD_LOGIC_VECTOR (13 downto 0) := "0000000000000"; 

begin
	
	SYNC_PROC: process (clk_dcm)
   begin
      if (clk_dcm'event and clk_dcm = '1') then
         if (reset = '1') then
				clk_eno <= 0; -- !! On arrete toute la logique (modules dac + mem) ? !!
            counter <= 0;
            state <= st_nstart;
         else
            state <= next_state;
         end if;        
      end if;
   end process;
 
   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
		if state = st_set_division then
         buff_freq_division <= to_integer(unsigned(data_in));
      elsif state = st_generate_clk then
         if (counter >= buff_freq_division) then
				clk_eno <= 1;
				counter <= 0;
			else
				clk_eno <= 0;
				counter <= counter + 1;
			end if;
      end if;
   end process;
 
   NEXT_STATE_DECODE: process (state, control)
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state

      case (state) is
			-- ETAT STOP --
         when st_nstart =>
				if control = x"84" then -- Marche du generateur
					next_state <= st_generate_clk;
				elsif control = x"87" then -- Diviseur de la frequence de lecture
					next_state <= st_set_division;
				end if;
			when st_set_division =>
				next_state <= st_nstart;
         -- ETAT START --
			when st_generate_clk =>
				if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_generate_clk;
				end if;
				
			when others =>
            next_state <= st_nstart;
      end case;      
   end process;

end Arch_main;

