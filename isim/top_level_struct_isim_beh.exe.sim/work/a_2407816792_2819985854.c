/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/M1/Xilinx/ELE680_Function_generator/module_memory_v2.vhd";
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_1547198987_1035706684(char *, char *, char *, char *, char *, char *);
char *ieee_p_1242562249_sub_1919365254_1035706684(char *, char *, char *, char *, int );
char *ieee_p_1242562249_sub_2540846514_1035706684(char *, char *, char *, char *, int );


static void work_a_2407816792_2819985854_p_0(char *t0)
{
    char t36[16];
    char t37[16];
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    int t17;
    int t18;
    int t19;
    int t20;
    int t22;
    char *t23;
    int t25;
    char *t26;
    char *t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    unsigned int t38;

LAB0:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 992U);
    t3 = xsi_signal_has_event(t2);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:
LAB3:    t2 = (t0 + 6776);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(69, ng0);
    t4 = (t0 + 1352U);
    t8 = *((char **)t4);
    t9 = *((unsigned char *)t8);
    t10 = (t9 == (unsigned char)3);
    if (t10 != 0)
        goto LAB8;

LAB10:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 1512U);
    t4 = *((char **)t2);
    t2 = (t0 + 12285);
    t17 = xsi_mem_cmp(t2, t4, 8U);
    if (t17 == 1)
        goto LAB12;

LAB19:    t8 = (t0 + 12293);
    t18 = xsi_mem_cmp(t8, t4, 8U);
    if (t18 == 1)
        goto LAB13;

LAB20:    t12 = (t0 + 12301);
    t19 = xsi_mem_cmp(t12, t4, 8U);
    if (t19 == 1)
        goto LAB14;

LAB21:    t14 = (t0 + 12309);
    t20 = xsi_mem_cmp(t14, t4, 8U);
    if (t20 == 1)
        goto LAB15;

LAB22:    t16 = (t0 + 12317);
    t22 = xsi_mem_cmp(t16, t4, 8U);
    if (t22 == 1)
        goto LAB16;

LAB23:    t23 = (t0 + 12325);
    t25 = xsi_mem_cmp(t23, t4, 8U);
    if (t25 == 1)
        goto LAB17;

LAB24:
LAB18:    xsi_set_current_line(94, ng0);
    t2 = (t0 + 12334);
    t5 = (t0 + 7288);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 1U);
    xsi_driver_first_trans_fast_port(t5);

LAB11:
LAB9:    goto LAB3;

LAB5:    t4 = (t0 + 1032U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB7;

LAB8:    xsi_set_current_line(70, ng0);
    t4 = (t0 + 12216);
    t12 = (t0 + 6904);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t4, 15U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(71, ng0);
    t2 = (t0 + 12231);
    t5 = (t0 + 6968);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 15U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 12246);
    t5 = (t0 + 7032);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 14U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 12260);
    t5 = (t0 + 7096);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 15U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 12275);
    t5 = (t0 + 7160);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(75, ng0);
    t2 = (t0 + 12279);
    t5 = (t0 + 7224);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 6U);
    xsi_driver_first_trans_fast_port(t5);
    goto LAB9;

LAB12:    xsi_set_current_line(79, ng0);
    t26 = (t0 + 1672U);
    t27 = *((char **)t26);
    t28 = (15 - 14);
    t29 = (t28 * 1U);
    t30 = (0 + t29);
    t26 = (t27 + t30);
    t31 = (t0 + 7096);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    t34 = (t33 + 56U);
    t35 = *((char **)t34);
    memcpy(t35, t26, 15U);
    xsi_driver_first_trans_fast(t31);
    goto LAB11;

LAB13:    xsi_set_current_line(81, ng0);
    t2 = (t0 + 12333);
    t5 = (t0 + 7288);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 1U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 3912U);
    t4 = *((char **)t2);
    t2 = (t0 + 7352);
    t5 = (t2 + 56U);
    t8 = *((char **)t5);
    t11 = (t8 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t4, 15U);
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 1672U);
    t4 = *((char **)t2);
    t28 = (15 - 13);
    t29 = (t28 * 1U);
    t30 = (0 + t29);
    t2 = (t4 + t30);
    t5 = (t0 + 7416);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 14U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 3912U);
    t4 = *((char **)t2);
    t2 = (t0 + 11968U);
    t5 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t36, t4, t2, 1);
    t8 = (t0 + 7096);
    t11 = (t8 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t5, 15U);
    xsi_driver_first_trans_fast(t8);
    goto LAB11;

LAB14:    xsi_set_current_line(86, ng0);
    t2 = (t0 + 1672U);
    t4 = *((char **)t2);
    t28 = (15 - 14);
    t29 = (t28 * 1U);
    t30 = (0 + t29);
    t2 = (t4 + t30);
    t5 = (t0 + 6968);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 15U);
    xsi_driver_first_trans_fast(t5);
    goto LAB11;

LAB15:    xsi_set_current_line(88, ng0);
    t2 = (t0 + 1672U);
    t4 = *((char **)t2);
    t28 = (15 - 3);
    t29 = (t28 * 1U);
    t30 = (0 + t29);
    t2 = (t4 + t30);
    t5 = (t0 + 7160);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_fast_port(t5);
    goto LAB11;

LAB16:    xsi_set_current_line(90, ng0);
    t2 = (t0 + 1672U);
    t4 = *((char **)t2);
    t28 = (15 - 13);
    t29 = (t28 * 1U);
    t30 = (0 + t29);
    t2 = (t4 + t30);
    t5 = (t0 + 7032);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 14U);
    xsi_driver_first_trans_fast(t5);
    goto LAB11;

LAB17:    xsi_set_current_line(92, ng0);
    t2 = (t0 + 1672U);
    t4 = *((char **)t2);
    t28 = (15 - 5);
    t29 = (t28 * 1U);
    t30 = (0 + t29);
    t2 = (t4 + t30);
    t5 = (t37 + 0U);
    t8 = (t5 + 0U);
    *((int *)t8) = 5;
    t8 = (t5 + 4U);
    *((int *)t8) = 0;
    t8 = (t5 + 8U);
    *((int *)t8) = -1;
    t17 = (0 - 5);
    t38 = (t17 * -1);
    t38 = (t38 + 1);
    t8 = (t5 + 12U);
    *((unsigned int *)t8) = t38;
    t8 = ieee_p_1242562249_sub_2540846514_1035706684(IEEE_P_1242562249, t36, t2, t37, 1);
    t11 = (t0 + 7224);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t8, 6U);
    xsi_driver_first_trans_fast_port(t11);
    goto LAB11;

LAB25:;
}

static void work_a_2407816792_2819985854_p_1(char *t0)
{
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(103, ng0);
    t2 = (t0 + 992U);
    t3 = xsi_signal_has_event(t2);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:
LAB3:    t2 = (t0 + 6792);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(104, ng0);
    t4 = (t0 + 1352U);
    t8 = *((char **)t4);
    t9 = *((unsigned char *)t8);
    t10 = (t9 == (unsigned char)3);
    if (t10 != 0)
        goto LAB8;

LAB10:    t2 = (t0 + 1192U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)3);
    if (t3 != 0)
        goto LAB11;

LAB12:
LAB9:    goto LAB3;

LAB5:    t4 = (t0 + 1032U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB7;

LAB8:    xsi_set_current_line(105, ng0);
    t4 = (t0 + 7480);
    t11 = (t4 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)0;
    xsi_driver_first_trans_fast(t4);
    goto LAB9;

LAB11:    xsi_set_current_line(107, ng0);
    t2 = (t0 + 3272U);
    t5 = *((char **)t2);
    t6 = *((unsigned char *)t5);
    t2 = (t0 + 7480);
    t8 = (t2 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = t6;
    xsi_driver_first_trans_fast(t2);
    goto LAB9;

}

static void work_a_2407816792_2819985854_p_2(char *t0)
{
    char t10[16];
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t11;
    char *t12;
    static char *nl0[] = {&&LAB5, &&LAB3, &&LAB4};

LAB0:    xsi_set_current_line(117, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 6808);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(119, ng0);
    t4 = (t0 + 4392U);
    t5 = *((char **)t4);
    t4 = (t0 + 7544);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t5, 15U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(120, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t1 = (t0 + 7608);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memcpy(t7, t2, 14U);
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(122, ng0);
    t1 = (t0 + 4232U);
    t2 = *((char **)t1);
    t1 = (t0 + 7672);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memcpy(t7, t2, 14U);
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(123, ng0);
    t1 = (t0 + 4552U);
    t2 = *((char **)t1);
    t1 = (t0 + 12032U);
    t4 = (t0 + 3752U);
    t5 = *((char **)t4);
    t4 = (t0 + 11952U);
    t6 = ieee_p_1242562249_sub_1547198987_1035706684(IEEE_P_1242562249, t10, t2, t1, t5, t4);
    t7 = (t0 + 7736);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t6, 15U);
    xsi_driver_first_trans_fast(t7);
    goto LAB2;

LAB4:    xsi_set_current_line(125, ng0);
    t1 = (t0 + 4552U);
    t2 = *((char **)t1);
    t1 = (t0 + 7544);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memcpy(t7, t2, 15U);
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(126, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t1 = (t0 + 7800);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memcpy(t7, t2, 14U);
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(128, ng0);
    t1 = (t0 + 4072U);
    t2 = *((char **)t1);
    t1 = (t0 + 7672);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memcpy(t7, t2, 14U);
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(129, ng0);
    t1 = (t0 + 4392U);
    t2 = *((char **)t1);
    t1 = (t0 + 12016U);
    t4 = (t0 + 3752U);
    t5 = *((char **)t4);
    t4 = (t0 + 11952U);
    t6 = ieee_p_1242562249_sub_1547198987_1035706684(IEEE_P_1242562249, t10, t2, t1, t5, t4);
    t7 = (t0 + 7864);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t6, 15U);
    xsi_driver_first_trans_fast(t7);
    goto LAB2;

LAB5:    xsi_set_current_line(131, ng0);
    t1 = (t0 + 12335);
    t4 = (t0 + 7864);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 15U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(132, ng0);
    t1 = (t0 + 12350);
    t4 = (t0 + 7736);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 15U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(133, ng0);
    t1 = (t0 + 12365);
    t4 = (t0 + 7608);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 14U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(134, ng0);
    t1 = (t0 + 12379);
    t4 = (t0 + 7800);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 14U);
    xsi_driver_first_trans_fast(t4);
    goto LAB2;

}

static void work_a_2407816792_2819985854_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned char t8;
    unsigned int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    static char *nl0[] = {&&LAB3, &&LAB4, &&LAB5};

LAB0:    xsi_set_current_line(142, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 7928);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(144, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 6824);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(146, ng0);
    t4 = (t0 + 1512U);
    t5 = *((char **)t4);
    t4 = (t0 + 12393);
    t8 = 1;
    if (8U == 8U)
        goto LAB10;

LAB11:    t8 = 0;

LAB12:    if (t8 != 0)
        goto LAB7;

LAB9:
LAB8:    goto LAB2;

LAB4:    xsi_set_current_line(150, ng0);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 12401);
    t3 = 1;
    if (8U == 8U)
        goto LAB19;

LAB20:    t3 = 0;

LAB21:    if (t3 != 0)
        goto LAB16;

LAB18:    xsi_set_current_line(153, ng0);
    t1 = (t0 + 7928);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB17:    goto LAB2;

LAB5:    xsi_set_current_line(156, ng0);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 12409);
    t3 = 1;
    if (8U == 8U)
        goto LAB28;

LAB29:    t3 = 0;

LAB30:    if (t3 != 0)
        goto LAB25;

LAB27:    xsi_set_current_line(159, ng0);
    t1 = (t0 + 7928);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);

LAB26:    goto LAB2;

LAB6:    xsi_set_current_line(163, ng0);
    t1 = (t0 + 7928);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB2;

LAB7:    xsi_set_current_line(147, ng0);
    t11 = (t0 + 7928);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    *((unsigned char *)t15) = (unsigned char)1;
    xsi_driver_first_trans_fast(t11);
    goto LAB8;

LAB10:    t9 = 0;

LAB13:    if (t9 < 8U)
        goto LAB14;
    else
        goto LAB12;

LAB14:    t7 = (t5 + t9);
    t10 = (t4 + t9);
    if (*((unsigned char *)t7) != *((unsigned char *)t10))
        goto LAB11;

LAB15:    t9 = (t9 + 1);
    goto LAB13;

LAB16:    xsi_set_current_line(151, ng0);
    t7 = (t0 + 7928);
    t10 = (t7 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)0;
    xsi_driver_first_trans_fast(t7);
    goto LAB17;

LAB19:    t9 = 0;

LAB22:    if (t9 < 8U)
        goto LAB23;
    else
        goto LAB21;

LAB23:    t5 = (t2 + t9);
    t6 = (t1 + t9);
    if (*((unsigned char *)t5) != *((unsigned char *)t6))
        goto LAB20;

LAB24:    t9 = (t9 + 1);
    goto LAB22;

LAB25:    xsi_set_current_line(157, ng0);
    t7 = (t0 + 7928);
    t10 = (t7 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)0;
    xsi_driver_first_trans_fast(t7);
    goto LAB26;

LAB28:    t9 = 0;

LAB31:    if (t9 < 8U)
        goto LAB32;
    else
        goto LAB30;

LAB32:    t5 = (t2 + t9);
    t6 = (t1 + t9);
    if (*((unsigned char *)t5) != *((unsigned char *)t6))
        goto LAB29;

LAB33:    t9 = (t9 + 1);
    goto LAB31;

}


extern void work_a_2407816792_2819985854_init()
{
	static char *pe[] = {(void *)work_a_2407816792_2819985854_p_0,(void *)work_a_2407816792_2819985854_p_1,(void *)work_a_2407816792_2819985854_p_2,(void *)work_a_2407816792_2819985854_p_3};
	xsi_register_didat("work_a_2407816792_2819985854", "isim/top_level_struct_isim_beh.exe.sim/work/a_2407816792_2819985854.didat");
	xsi_register_executes(pe);
}
