
-- VHDL Instantiation Created from source file module_memory.vhd -- 23:04:14 03/10/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT module_memory
	PORT(
		clk : IN std_logic;
		clk_eni : IN std_logic;
		reset : IN std_logic;
		control : IN std_logic_vector(7 downto 0);
		data_in : IN std_logic_vector(15 downto 0);
		mem_doutb : IN std_logic_vector(13 downto 0);          
		data_out : OUT std_logic_vector(13 downto 0);
		mem_dina : OUT std_logic_vector(13 downto 0);
		mem_wea : OUT std_logic_vector(0 to 0);
		mem_addra : OUT std_logic_vector(14 downto 0);
		mem_addrb : OUT std_logic_vector(14 downto 0);
		val_sgn_att : OUT std_logic_vector(3 downto 0);
		val_freq_divider : OUT std_logic_vector(5 downto 0)
		);
	END COMPONENT;

	Inst_module_memory: module_memory PORT MAP(
		clk => ,
		clk_eni => ,
		reset => ,
		control => ,
		data_in => ,
		data_out => ,
		mem_dina => ,
		mem_doutb => ,
		mem_wea => ,
		mem_addra => ,
		mem_addrb => ,
		val_sgn_att => ,
		val_freq_divider => 
	);


