----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:28:10 03/09/2014 
-- Design Name: 
-- Module Name:    module_dac - Arch_dac 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity module_dac is
    Port ( clk_dac : in  STD_LOGIC;
           control : in  STD_LOGIC_VECTOR (7 downto 0);
           reset : in  STD_LOGIC;
           data_in : in  STD_LOGIC_VECTOR (13 downto 0);
           data_out : out  STD_LOGIC_VECTOR (13 downto 0));
end module_dac;

architecture Arch_dac of module_dac is
	type state_type is (st_nstart, st_send_data1, st_send_data2); 
   signal state, next_state : state_type; 
	-- Ajout de latence
	signal buff_data1 : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000";
	signal buff_data2 : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; 


begin

	SYNC_PROC: process (clk_dac)
   begin
      if (clk_dac'event and clk_dac = '1') then
         if (reset = '1') then
            state <= st_nstart;
         else
            state <= next_state;
         end if;        
      end if;
   end process;
 
   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
      if state = st_send_data1 then
         buff_data2 <= data_in; -- mise en tampon du prochain signal
			data_out <= buff_data1; -- Envoi vers le dac
		elsif state = st_send_data2 then
         buff_data1 <= data_in;
			data_out <= buff_data2;
      end if;
   end process;
 
   NEXT_STATE_DECODE: process (state, control)
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state

      case (state) is
			-- ETAT STOP --
         when st_nstart =>
				if control = x"84" then -- Marche du generateur
					next_state <= st_send_data1;
				end if;
         -- ETAT SEND --
			when st_send_data1 =>
				if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_send_data2;
				end if;
			when st_send_data2 =>
				if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_send_data1;
				end if;
				
			when others =>
            next_state <= st_nstart;
      end case;      
   end process;

end Arch_dac;

