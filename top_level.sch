<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(13:0)" />
        <signal name="XLXN_2(0:0)" />
        <signal name="XLXN_3(14:0)" />
        <signal name="XLXN_4(14:0)" />
        <signal name="XLXN_5(13:0)" />
        <signal name="XLXN_6(15:0)" />
        <signal name="XLXN_7(7:0)" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10(13:0)" />
        <blockdef name="module_main">
            <timestamp>2014-3-10T1:47:21</timestamp>
            <rect width="304" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="432" y1="-288" y2="-288" x1="368" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <rect width="64" x="368" y="-108" height="24" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="module_dac">
            <timestamp>2014-3-10T1:53:48</timestamp>
            <rect width="304" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-172" height="24" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="module_memory">
            <timestamp>2014-3-10T1:47:8</timestamp>
            <rect width="368" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-256" y2="-256" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-76" height="24" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
            <rect width="64" x="432" y="-364" height="24" />
            <line x2="496" y1="-352" y2="-352" x1="432" />
            <rect width="64" x="432" y="-300" height="24" />
            <line x2="496" y1="-288" y2="-288" x1="432" />
            <rect width="64" x="432" y="-236" height="24" />
            <line x2="496" y1="-224" y2="-224" x1="432" />
            <rect width="64" x="432" y="-172" height="24" />
            <line x2="496" y1="-160" y2="-160" x1="432" />
            <rect width="64" x="432" y="-108" height="24" />
            <line x2="496" y1="-96" y2="-96" x1="432" />
            <rect width="64" x="432" y="-44" height="24" />
            <line x2="496" y1="-32" y2="-32" x1="432" />
        </blockdef>
        <blockdef name="RAM">
            <timestamp>2014-3-8T22:38:55</timestamp>
            <rect width="512" x="32" y="32" height="1344" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="112" y2="112" style="linewidth:W" x1="0" />
            <line x2="32" y1="208" y2="208" style="linewidth:W" x1="0" />
            <line x2="32" y1="272" y2="272" x1="0" />
            <line x2="32" y1="432" y2="432" style="linewidth:W" x1="0" />
            <line x2="32" y1="624" y2="624" x1="0" />
            <line x2="544" y1="368" y2="368" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="module_dac" name="XLXI_3">
            <blockpin signalname="XLXN_9" name="clk_dac" />
            <blockpin name="reset" />
            <blockpin name="control(7:0)" />
            <blockpin signalname="XLXN_10(13:0)" name="data_in(13:0)" />
            <blockpin name="data_out(13:0)" />
        </block>
        <block symbolname="module_main" name="XLXI_4">
            <blockpin name="clk_dcm" />
            <blockpin name="reset" />
            <blockpin signalname="XLXN_7(7:0)" name="control(7:0)" />
            <blockpin name="data_in(15:0)" />
            <blockpin name="clk_dac" />
            <blockpin signalname="XLXN_8" name="clk_mod_mem" />
            <blockpin signalname="XLXN_9" name="clk_mod_dac" />
            <blockpin signalname="XLXN_6(15:0)" name="data_out(15:0)" />
        </block>
        <block symbolname="module_memory" name="XLXI_5">
            <blockpin signalname="XLXN_8" name="clk_mem" />
            <blockpin name="reset" />
            <blockpin signalname="XLXN_7(7:0)" name="control(7:0)" />
            <blockpin signalname="XLXN_6(15:0)" name="data_in(15:0)" />
            <blockpin signalname="XLXN_5(13:0)" name="mem_doutb(13:0)" />
            <blockpin signalname="XLXN_10(13:0)" name="data_out(13:0)" />
            <blockpin signalname="XLXN_1(13:0)" name="mem_dina(13:0)" />
            <blockpin signalname="XLXN_2(0:0)" name="mem_wea(0:0)" />
            <blockpin signalname="XLXN_3(14:0)" name="mem_addra(14:0)" />
            <blockpin signalname="XLXN_4(14:0)" name="mem_addrb(14:0)" />
        </block>
        <block symbolname="RAM" name="XLXI_6">
            <blockpin signalname="XLXN_3(14:0)" name="addra(14:0)" />
            <blockpin signalname="XLXN_1(13:0)" name="dina(13:0)" />
            <blockpin signalname="XLXN_2(0:0)" name="wea(0:0)" />
            <blockpin name="clka" />
            <blockpin signalname="XLXN_4(14:0)" name="addrb(14:0)" />
            <blockpin name="clkb" />
            <blockpin signalname="XLXN_5(13:0)" name="doutb(13:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="2064" y="592" name="XLXI_3" orien="R0">
        </instance>
        <instance x="784" y="1056" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2752" y="528" name="XLXI_6" orien="R0">
        </instance>
        <branch name="XLXN_1(13:0)">
            <wire x2="2512" y1="1056" y2="1056" x1="2288" />
            <wire x2="2512" y1="640" y2="1056" x1="2512" />
            <wire x2="2752" y1="640" y2="640" x1="2512" />
        </branch>
        <branch name="XLXN_2(0:0)">
            <wire x2="2528" y1="1120" y2="1120" x1="2288" />
            <wire x2="2528" y1="736" y2="1120" x1="2528" />
            <wire x2="2752" y1="736" y2="736" x1="2528" />
        </branch>
        <branch name="XLXN_3(14:0)">
            <wire x2="2592" y1="1184" y2="1184" x1="2288" />
            <wire x2="2752" y1="608" y2="608" x1="2592" />
            <wire x2="2592" y1="608" y2="1184" x1="2592" />
        </branch>
        <branch name="XLXN_4(14:0)">
            <wire x2="2736" y1="1248" y2="1248" x1="2288" />
            <wire x2="2752" y1="960" y2="960" x1="2736" />
            <wire x2="2736" y1="960" y2="1248" x1="2736" />
        </branch>
        <branch name="XLXN_5(13:0)">
            <wire x2="1792" y1="1280" y2="1280" x1="1712" />
            <wire x2="1712" y1="1280" y2="1984" x1="1712" />
            <wire x2="3344" y1="1984" y2="1984" x1="1712" />
            <wire x2="3344" y1="896" y2="896" x1="3328" />
            <wire x2="3344" y1="896" y2="1984" x1="3344" />
        </branch>
        <branch name="XLXN_6(15:0)">
            <wire x2="1488" y1="960" y2="960" x1="1216" />
            <wire x2="1488" y1="960" y2="1184" x1="1488" />
            <wire x2="1792" y1="1184" y2="1184" x1="1488" />
        </branch>
        <branch name="XLXN_7(7:0)">
            <wire x2="1232" y1="1024" y2="1024" x1="1216" />
            <wire x2="1232" y1="1024" y2="1408" x1="1232" />
            <wire x2="2368" y1="1408" y2="1408" x1="1232" />
            <wire x2="2368" y1="1312" y2="1312" x1="2288" />
            <wire x2="2368" y1="1312" y2="1408" x1="2368" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1504" y1="832" y2="832" x1="1216" />
            <wire x2="1504" y1="832" y2="992" x1="1504" />
            <wire x2="1792" y1="992" y2="992" x1="1504" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1632" y1="896" y2="896" x1="1216" />
            <wire x2="1632" y1="432" y2="896" x1="1632" />
            <wire x2="2064" y1="432" y2="432" x1="1632" />
        </branch>
        <branch name="XLXN_10(13:0)">
            <wire x2="2064" y1="560" y2="560" x1="2048" />
            <wire x2="2048" y1="560" y2="640" x1="2048" />
            <wire x2="2352" y1="640" y2="640" x1="2048" />
            <wire x2="2352" y1="640" y2="992" x1="2352" />
            <wire x2="2352" y1="992" y2="992" x1="2288" />
        </branch>
        <instance x="1792" y="1344" name="XLXI_5" orien="R0">
        </instance>
    </sheet>
</drawing>