
-- VHDL Instantiation Created from source file module_dac.vhd -- 23:04:22 03/10/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT module_dac
	PORT(
		clk : IN std_logic;
		control : IN std_logic_vector(7 downto 0);
		reset : IN std_logic;
		val_sgn_att : IN std_logic_vector(3 downto 0);
		data_in : IN std_logic_vector(13 downto 0);          
		data_out : OUT std_logic_vector(13 downto 0)
		);
	END COMPONENT;

	Inst_module_dac: module_dac PORT MAP(
		clk => ,
		control => ,
		reset => ,
		val_sgn_att => ,
		data_in => ,
		data_out => 
	);


