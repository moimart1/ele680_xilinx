----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:07:09 03/10/2014 
-- Design Name: 
-- Module Name:    top_level_struct - Structure 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_level_struct is
	 Port ( clk_in : in  STD_LOGIC;
			  clk_dac : out  STD_LOGIC;
			  reset : in  STD_LOGIC;
           control : in  STD_LOGIC_VECTOR (7 downto 0);
			  data_from_ft232 : in  STD_LOGIC_VECTOR (15 downto 0);
           data_to_dac : out STD_LOGIC_VECTOR (13 downto 0)); 
end top_level_struct;

architecture Structure of top_level_struct is

	component DCM
	port
	 (-- Clock in ports
	  CLK_IN1           : in     std_logic;
	  -- Clock out ports
	  CLK_OUT          : out    std_logic;
	  CLK_OUT_DAC          : out    std_logic;
	  -- Status and control signals
	  RESET             : in     std_logic;
	  LOCKED            : out    std_logic
	 );
	end component;
	
	COMPONENT RAM
	  PORT (
		 clka : IN STD_LOGIC;
		 wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		 addra : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
		 dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
		 clkb : IN STD_LOGIC;
		 addrb : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
		 doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
	  );
	END COMPONENT;
	
	COMPONENT module_main
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		control : IN std_logic_vector(7 downto 0);
		val_freq_divider : IN std_logic_vector(5 downto 0);          
		clk_eno : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT module_memory
	PORT(
		clk : IN std_logic;
		clk_eni : IN std_logic;
		reset : IN std_logic;
		control : IN std_logic_vector(7 downto 0);
		data_in : IN std_logic_vector(15 downto 0);
		mem_doutb : IN std_logic_vector(13 downto 0);          
		data_out : OUT std_logic_vector(13 downto 0);
		mem_dina : OUT std_logic_vector(13 downto 0);
		mem_wea : OUT std_logic_vector(0 to 0);
		mem_addra : OUT std_logic_vector(14 downto 0);
		mem_addrb : OUT std_logic_vector(14 downto 0);
		val_sgn_att : OUT std_logic_vector(3 downto 0);
		val_freq_divider : OUT std_logic_vector(5 downto 0)
		);
	END COMPONENT;

	COMPONENT module_dac
	PORT(
		clk : IN std_logic;
		control : IN std_logic_vector(7 downto 0);
		reset : IN std_logic;
		val_sgn_att : IN std_logic_vector(3 downto 0);
		data_in : IN std_logic_vector(13 downto 0);          
		data_out : OUT std_logic_vector(13 downto 0)
		);
	END COMPONENT;
	
	signal clk : STD_LOGIC;
	signal mem_wea : STD_LOGIC_VECTOR (0 downto 0);
	signal mem_addra : STD_LOGIC_VECTOR (14 downto 0);
	signal mem_addrb : STD_LOGIC_VECTOR (14 downto 0);
	signal mem_dina : STD_LOGIC_VECTOR (13 downto 0);
	signal mem_doutb : STD_LOGIC_VECTOR (13 downto 0);
	signal val_freq_divider : STD_LOGIC_VECTOR (5 downto 0);
	signal clk_en : STD_LOGIC;
	signal data_from_mem : STD_LOGIC_VECTOR (13 downto 0);
	signal val_sgn_att : STD_LOGIC_VECTOR (3 downto 0);
	signal LOCKED : STD_LOGIC;

begin
	
	Inst_DCM : DCM
	  port map
		(-- Clock in ports
		 clk_in,
		 -- Clock out ports
		 clk,
		 clk_dac,
		 -- Status and control signals
		 reset,
		 LOCKED);
	
	Inst_RAM : RAM
	  PORT MAP (
		 clk,
		 mem_wea,
		 mem_addra,
		 mem_dina,
		 clk,
		 mem_addrb,
		 mem_doutb
	  );
	
	Inst_module_main: module_main PORT MAP(
		clk,
		reset,
		control,
		val_freq_divider,
		clk_en
	);
	
	Inst_module_memory: module_memory PORT MAP(
		clk,
		clk_en,
		reset,
		control,
		data_from_ft232,
		data_from_mem,
		mem_dina,
		mem_doutb,
		mem_wea,
		mem_addra,
		mem_addrb,
		val_sgn_att,
		val_freq_divider
	);
	
	Inst_module_dac: module_dac PORT MAP(
		clk,
		control,
		reset,
		val_sgn_att,
		data_from_mem,
		data_to_dac
	);

end Structure;

