
-- VHDL Instantiation Created from source file module_main.vhd -- 23:03:14 03/10/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT module_main
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		control : IN std_logic_vector(7 downto 0);
		val_freq_divider : IN std_logic_vector(5 downto 0);          
		clk_eno : OUT std_logic
		);
	END COMPONENT;

	Inst_module_main: module_main PORT MAP(
		clk => ,
		reset => ,
		control => ,
		val_freq_divider => ,
		clk_eno => 
	);


