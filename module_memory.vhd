----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:58:49 03/08/2014 
-- Design Name: 
-- Module Name:    module_memory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity module_memory is
    Port ( clk: in  STD_LOGIC;
			  clk_eni: in STD_LOGIC;
			  reset : in  STD_LOGIC;
           control : in  STD_LOGIC_VECTOR (7 downto 0);
           data_in : in  STD_LOGIC_VECTOR (15 downto 0);
           data_out : out  STD_LOGIC_VECTOR (13 downto 0);
           mem_dina : out  STD_LOGIC_VECTOR (13 downto 0);
           mem_doutb : in  STD_LOGIC_VECTOR (13 downto 0);
           mem_wea : out  STD_LOGIC_VECTOR (0 downto 0);
           mem_addra : out  STD_LOGIC_VECTOR (14 downto 0);
           mem_addrb : out  STD_LOGIC_VECTOR (14 downto 0));
end module_memory;

architecture Arch_memory of module_memory is

	type state_type is (st_nstart, st_start, st_set_addr_first, st_set_data, st_set_addr_last, st_set_addr_saut, set_signal_att, 
								st_addr_increment, st_addr_next, st_signal_read1, st_signal_read2); 
   signal state, next_state : state_type; 

	signal buff_addr_first : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- !! SUPPRIMER ? !! -- premiere adresse d'ecriture ds la memoire
	signal buff_addr_last : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- dernier adresse d'ecriture ds la memoire
	signal buff_addr_saut : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; -- valeur du saut en memoire
	signal buff_addr_current : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- pointeur vers l'adresse courante en ecriture
	signal buff_signal_att : integer range 0 to 15 := 0; -- valeur de l'attenuation du signal (diviseur)
	signal buff_signal1 : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; -- tampon1 du signal
	signal buff_signal2 : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; -- tampon2 du signal
	signal buff_addr_sgn1 : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- pointeur vers l'adresse pour le signal1 en lecture
	signal buff_addr_sgn2 : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- pointeur vers l'adresse pour le signal2 en lecture
	

begin

	SYNC_PROC: process (clk)
   begin
      if (clk'event and clk= '1') then
         if (reset = '1') then
            state <= st_nstart;
         elsif(clk_eni = '1') then
            state <= next_state;
         end if;        
      end if;
   end process;
 
   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
      --below is simple example
		case (state) is 
      when st_set_addr_first =>
         buff_addr_current <= data_in(14 DOWNTO 0);
      when st_set_addr_last =>
         buff_addr_last <= data_in(14 DOWNTO 0);
      when st_set_addr_saut =>
         buff_addr_saut <= data_in(13 DOWNTO 0);
      when st_set_data =>
         mem_wea <= "1"; --Activation de l'ecriture
			mem_addra <= buff_addr_current; -- Adresse d'ecriture
			mem_dina <= data_in(13 DOWNTO 0);
			buff_addr_current <= std_logic_vector(unsigned(buff_addr_current) + 1); -- !! Overflow ? Et prend du temps ? !!
      when set_signal_att =>
         buff_signal_att <= to_integer(unsigned(data_in(3 DOWNTO 0)));
		when st_start =>
         buff_addr_sgn1 <= x"0000";--buff_addr_first; --0 A METTRE;
			buff_addr_sgn2 <= x"0000";--buff_addr_first; --0 A METTRE;
		when st_signal_read1 =>
         mem_wea <= "0"; -- Activation de la lecture
			mem_addrb <= buff_addr_sgn1; -- Adresse de lecture
			buff_signal1 <= std_logic_vector(unsigned(mem_doutb) srl buff_signal_att);
			data_out <= buff_signal2;
			buff_addr_sgn2 <= std_logic_vector(unsigned(buff_addr_sgn2) + unsigned(buff_addr_saut));  -- !! Overflow ? !! -- Preparation de l'adresse de lecture 
		when st_signal_read2 =>
         mem_wea <= "0"; -- Activation de la lecture
			mem_addrb <= buff_addr_sgn2; -- Adresse de lecture
			buff_signal2 <= std_logic_vector(unsigned(mem_doutb) srl buff_signal_att);
			data_out <= buff_signal1;
			buff_addr_sgn1 <= buff_addr_sgn1;-- + buff_addr_saut;  -- !! Overflow ? !! -- Preparation de l'adresse de lecture
		--when others =>
         --<statement>;
		end case;

   end process;
 
   NEXT_STATE_DECODE: process (state, control)
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state

      case (state) is
			-- ETAT STOP --
         when st_nstart =>
				if control = x"84" then -- Marche du generateur
					next_state <= st_start;
				else
					if control = x"80" then -- Adresse d'ecriture
						next_state <= st_set_addr_first;
					elsif control = x"81" then -- Chargement donnees
						next_state <= st_set_data;
					elsif control = x"82" then -- Adresse de lecture maximale
						next_state <= st_set_addr_last;
					elsif control = x"86" then -- Saut de l'adresse de lecture
						next_state <= st_set_addr_saut;
					end if;
				end if;
			when st_set_data => -- etat pour incrementer le pointeur
            next_state <= st_addr_increment;
			-- Retour vers etat stop --
			when st_addr_increment =>
            next_state <= st_nstart;
			when st_set_addr_first =>
            next_state <= st_nstart;
			when st_set_addr_last =>
            next_state <= st_nstart;
			when st_set_addr_saut =>
            next_state <= st_nstart;
			when set_signal_att =>
            next_state <= st_nstart;
         -- ETAT START --
			when st_start =>
				if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_signal_read1; -- vers 1er tampon
				end if;
			when st_signal_read1 =>
            if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_signal_read2; -- Changement de tampon
				end if;
			when st_signal_read2 =>
            if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_signal_read1; -- Changement de tampon
				end if;
				
			when others =>
            next_state <= st_nstart;
      end case;      
   end process;


end Arch_memory;

