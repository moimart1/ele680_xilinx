---------------------------------------------------------------------------------
--                                      DECODE_FT232H                          
-- Realise par : Amaury LAINE  											 
-- Le : 03/03/2014                                                             

-- Description :                                                               
--    Permet de realiser l'interface entre le FPGA et le circuit integre FT232H
--  																								    
-- INPUT :																							 
-- 	-	RXF_i : '0' indique qu'une donnee est presente sur USBDATA_io
--					  '1' ne pas lire la donnee	
-- 	-	TXE_i : '0' indique qu'une donnee est presente sur USBDATA_io
-- 				  '1' USBDATA_io pas libre et ne peut pas etre ecrit	
-- 	-	RESET_i : '1' Reset les sorties en mode synchrone
-- 				  	 '0' fonctionnement normal		
-- OUTPUT :	
-- 	-	RD_o 	  : '0' En train de lire une donnee
-- 				  	 '1' Lecteure finie et bien receptionnee
-- 	-	WR_o 	  : '0' Demande d'ecriture sur USBDATA_io, ok si TXE_i ='1'
-- 				  	 '1' Pas de demande d'�criture.	
-- 	-	USBDATA_o 	  : donnee extraite de la trame en fonction 
--								 de la commande sur 16 bits
-- 	-	CONTROLE_o 	  : controle correspond � la commande de la trame 
--
--	INOUTPUT :
--		-	USBDATA_io : Bus de donn�es sur 8 bits provenant du FT232H														 
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DECODE_FT232H is
    Port ( RXF_i : in  STD_LOGIC;
           TXE_i : in  STD_LOGIC;
           CLK_i : in  STD_LOGIC;
           RESET_i : in  STD_LOGIC;
           RD_o : out  STD_LOGIC := '1';
           WR_o : out  STD_LOGIC := '1';
           USBDATA_o : out  STD_LOGIC_VECTOR (15 downto 0);
			  USBDATA_io : inout  STD_LOGIC_VECTOR (7 downto 0);
			  CONTROLE_o : out  STD_LOGIC_VECTOR (7 downto 0) :="00000000");
end DECODE_FT232H;

ARCHITECTURE DECODE_FT232H_archi of DECODE_FT232H IS
SIGNAL Buff_Controle : STD_LOGIC_VECTOR (7 downto 0):="00000000"; -- memoriser l'etat de commande
SIGNAL Quantite_N : STD_LOGIC_VECTOR (7 downto 0):="00000000"; 	-- Quantite d'octet de donnee dans la trame
SIGNAL Sreg, Snext : STD_LOGIC_VECTOR (2 downto 0);   				-- Etat present et etat suivant de la machine a etat

BEGIN
	PROCESS(CLK_i)
		BEGIN
			IF rising_edge(CLK_i) THEN
				IF RESET_i = '1' THEN		--sortie mise � zero lors d'un RESET_i
					Snext <= "000";		
				ELSIF RXF_i = '0' THEN		--donnee presente sur USBDATA_io
					CASE Sreg IS
					  -- INITIALISATION
						WHEN "000" => 			IF USBDATA_io = "01110001" THEN Snext <= "001";		--si commande 0x71 Start trame
													ELSE Snext <= "000";										
													END IF;
					  -- START TRAME
						WHEN "001" => 		   IF USBDATA_io >= "10000000" AND USBDATA_io <= "01110111" THEN Snext <= "010";		--si commande entre 0x80 et 0x87
													ELSE Snext <= "000";										
													END IF;			  
					  -- COMMANDE
						WHEN "010" => 		   IF USBDATA_io >= "00000010" THEN Snext <= "011";	--si quantite superieur a 2
													ELSE Snext <= "000";										
													END IF;
					  -- Enregistrement quantit� Bytes DATA
						WHEN "011" => 		   Snext <= "100";
													
					  -- Enregistrement MSB
						WHEN "100" => 		   IF Quantite_N = "00000000" THEN Snext <= "101";		--si Quantite_N = 0 erreur du nombre de donnees
													ELSE Snext <= "000";										
													END IF;
					  -- Enregistrement LSB
						WHEN "101" => 		   IF 	Quantite_N = "00000000" AND TXE_i='0'  THEN Snext <= "110";		--si Quantite_N = 0 et bus de donn�e vide, confirmer
													ELSIF Quantite_N /= "00000000" THEN Snext <= "100";						--si Quantite_N != 0 traitement pas fini
													ELSE Snext <= "000";										
													END IF;
					  -- Confirmation
						WHEN "110" => 		   IF TXE_i='0' THEN Snext <= "000";		--si Quantite_N = 0 erreur du nombre de donnees
													ELSE Snext <= "110";										
													END IF;			
					  -- AUTRES ETATS
						WHEN OTHERS => Snext <= "000"; 
				END CASE;		
			END IF;
		END IF;
	END PROCESS;
	
	Sreg <= Snext;
	
WITH Sreg SELECT -- logique de sortie RD_o
	RD_o 			  				  <=  '0' WHEN "000" | "001" | "010" | "011" | "100" | "101",
											'1' WHEN "110",
											'1' WHEN OTHERS;
											
WITH Sreg SELECT -- logique de sortie WR_o
	WR_o 			  				  <=  '0' WHEN "110",
											'1' WHEN "000" | "001" | "010" | "011" | "100" | "101",
											'1' WHEN OTHERS;
											
WITH Sreg SELECT -- logique de sortie USBDATA_o MSB
	USBDATA_o(13 downto 8) 	  <=  USBDATA_io(5 downto 0) 	 	WHEN "100",
											"000000" 						WHEN "000";
											
WITH Sreg SELECT -- logique de sortie USBDATA_o LSB
	USBDATA_o(7 downto 0) 	  <=  USBDATA_io(7 downto 0) 	 	WHEN "101",
											x"00" 			    		 	WHEN "000";							

WITH Sreg SELECT -- logique de sortie Buff_Controle
	Buff_Controle(7 downto 0) <=  USBDATA_io(7 downto 0) 		WHEN "010",
											x"00"				 				WHEN "000";
											
WITH Sreg SELECT -- logique de sortie Quantite_N
	Quantite_N(7 downto 0)	  <=  USBDATA_io(7 downto 0) 		WHEN "011",
											x"00"								WHEN "000";
											
WITH Sreg SELECT -- logique de sortie USBDATA_io
	USBDATA_io(7 downto 0)	  <=  Buff_Controle(7 downto 0) 	WHEN "110";
END DECODE_FT232H_archi;  
