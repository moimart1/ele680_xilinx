----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:14:58 03/09/2014 
-- Design Name: 
-- Module Name:    module_main - Arch_main 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity module_main is
    Port ( clk : in  STD_LOGIC;
			  reset : in  STD_LOGIC;
           control : in  STD_LOGIC_VECTOR (7 downto 0);
			  val_freq_divider : in  STD_LOGIC_VECTOR (5 downto 0);
           clk_eno : out STD_LOGIC); 
end module_main;

architecture Arch_main of module_main is

	type state_type is (st_nstart, st_generate_clk); 
   signal state, next_state : state_type; 

   signal counter : integer range 0 to 63 := 0;
	
begin
	
	SYNC_PROC: process (clk)
   begin
      if (clk'event and clk = '1') then
         if (reset = '1') then
            state <= st_nstart;
         else
            state <= next_state;
         end if;        
      end if;
   end process;
 
   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
		if state = st_nstart then
			counter <= 0;
			clk_eno <= '1';
      elsif state = st_generate_clk then
         if (counter >= to_integer(unsigned(val_freq_divider))) then
				clk_eno <= '1';
				counter <= 0;
			else
				clk_eno <= '0';
				counter <= counter + 1;
			end if;
      end if;
   end process;
 
   NEXT_STATE_DECODE: process (state, control)
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state

      case (state) is
			-- ETAT STOP --
         when st_nstart =>
				if control = x"84" then -- Marche du generateur
					next_state <= st_generate_clk;
				end if;
         -- ETAT START --
			when st_generate_clk =>
				if control = x"83" then -- Arret generateur
					next_state <= st_nstart;
				else
					next_state <= st_generate_clk;
				end if;
				
			when others =>
            next_state <= st_nstart;
      end case;      
   end process;

end Arch_main;

