----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:58:49 03/08/2014 
-- Design Name: 
-- Module Name:    module_memory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity module_memory is
    Port ( clk: in  STD_LOGIC;
			  clk_eni: in STD_LOGIC;
			  reset : in  STD_LOGIC;
           control : in  STD_LOGIC_VECTOR (7 downto 0);
           data_in : in  STD_LOGIC_VECTOR (15 downto 0);
           data_out : out  STD_LOGIC_VECTOR (13 downto 0);
           mem_dina : out  STD_LOGIC_VECTOR (13 downto 0);
           mem_doutb : in  STD_LOGIC_VECTOR (13 downto 0);
           mem_wea : out  STD_LOGIC_VECTOR (0 downto 0);
           mem_addra : out  STD_LOGIC_VECTOR (14 downto 0);
           mem_addrb : out  STD_LOGIC_VECTOR (14 downto 0);
			  val_sgn_att : out  STD_LOGIC_VECTOR (3 downto 0);
			  val_freq_divider : out STD_LOGIC_VECTOR (5 downto 0));
end module_memory;

architecture Arch_memory of module_memory is

	type state_type is (st_init, st_signal_read1, st_signal_read2); 
   signal state, next_state : state_type; 

	signal buff_addr_first : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- !! SUPPRIMER ? !! -- premiere adresse d'ecriture ds la memoire
	signal buff_addr_last : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- dernier adresse d'ecriture ds la memoire
	signal buff_addr_saut : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; -- valeur du saut en memoire
	signal buff_addr_current : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- pointeur vers l'adresse courante en ecriture
	signal buff_signal1 : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; -- tampon1 du signal
	signal buff_signal2 : STD_LOGIC_VECTOR (13 downto 0) := "00000000000000"; -- tampon2 du signal
	signal buff_addr_sgn1 : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- pointeur vers l'adresse pour le signal1 en lecture
	signal buff_addr_sgn2 : STD_LOGIC_VECTOR (14 downto 0) := "000000000000000"; -- pointeur vers l'adresse pour le signal2 en lecture
	

begin
	
	BUFF_SET: process (clk)
   begin
      if (clk'event and clk= '1') then
         if (reset = '1') then
            buff_addr_first <= "000000000000000";
				buff_addr_last <= "000000000000000";
				buff_addr_saut <= "00000000000000";
				buff_addr_current <= "000000000000000";
				val_sgn_att <= "0000";
				val_freq_divider <= "000000";
         else
            case (control) is 
				when x"80" => -- Adresse d'ecriture
					buff_addr_current <= data_in(14 DOWNTO 0);
				when x"81" => -- Chargement des donnees
					mem_wea <= "1"; --Activation de l'ecriture
					mem_addra <= buff_addr_current; -- Adresse d'ecriture
					mem_dina <= data_in(13 DOWNTO 0);
					buff_addr_current <= std_logic_vector(unsigned(buff_addr_current) + 1); -- !! Overflow ? Et prend du temps ? !!
				when x"82" => -- Adresse de lecture maximale
					buff_addr_last <= data_in(14 DOWNTO 0);
				when x"85" => -- Attenuation du signal
					val_sgn_att <= data_in(3 DOWNTO 0);
				when x"86" => -- Saut de l'adresse de lecture
					buff_addr_saut <= data_in(13 DOWNTO 0);
				when x"87" => -- Diviseur de frequence
					val_freq_divider <= std_logic_vector(unsigned(data_in(5 DOWNTO 0)) sll 1); --Multiplie par 2
				when others =>
					mem_wea <= "0"; --Activation de la lecture
					-- Sinon rien n'est fait
				end case;
         end if;        
      end if;
   end process;
	
	SYNC_PROC: process (clk)
   begin
      if (clk'event and clk = '1') then
         if (reset = '1') then
            state <= st_init;
         elsif(clk_eni = '1') then
            state <= next_state;
         end if;        
      end if;
   end process;
 
   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
      --below is simple example
		case (state) is    
		when st_signal_read1 =>
			mem_addrb <= buff_addr_sgn1; -- Adresse de lecture
			buff_signal1 <= mem_doutb;
			--buff_signal1 <= std_logic_vector(unsigned(mem_doutb) srl buff_signal_att);
			data_out <= buff_signal2;
			buff_addr_sgn2 <= std_logic_vector(unsigned(buff_addr_sgn2) + unsigned(buff_addr_saut));  -- !! Overflow ? !! -- Preparation de l'adresse de lecture 
		when st_signal_read2 =>
			mem_addrb <= buff_addr_sgn2; -- Adresse de lecture
			buff_signal2 <= mem_doutb;
			--buff_signal2 <= std_logic_vector(unsigned(mem_doutb) srl buff_signal_att);
			data_out <= buff_signal1;
			buff_addr_sgn1 <= std_logic_vector(unsigned(buff_addr_sgn1) + unsigned(buff_addr_saut));  -- !! Overflow ? !! -- Preparation de l'adresse de lecture
		when others =>
         buff_addr_sgn1 <= "000000000000000";
			buff_addr_sgn2 <= "000000000000000";
			buff_signal1 <= "00000000000000";
			buff_signal2 <= "00000000000000";
		end case;

   end process;
 
   NEXT_STATE_DECODE: process (state, control)
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state

      case (state) is
			when st_init =>
				if control = x"84" then -- Marche generateur
					next_state <= st_signal_read1;
				end if;
			when st_signal_read1 =>
            if control = x"83" then -- Arret generateur
					next_state <= st_init;
				else
					next_state <= st_signal_read2; -- Changement de tampon
				end if;
			when st_signal_read2 =>
            if control = x"83" then -- Arret generateur
					next_state <= st_init;
				else
					next_state <= st_signal_read1; -- Changement de tampon
				end if;
				
			when others =>
            next_state <= st_init;
      end case;      
   end process;

end Arch_memory;

